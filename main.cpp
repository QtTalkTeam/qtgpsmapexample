#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "lib/ql-channel-serial.hpp"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<QlChannelSerial>("QlChannelSerial", 1,0, "QlChannelSerial");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
