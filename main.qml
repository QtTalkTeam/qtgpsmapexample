import QtQuick 2.6
import QtQuick.Window 2.2

import QtLocation 5.6
import QtPositioning 5.6
import QlChannelSerial 1.0

Window {
    visible: true
    width:   640
    height:  480
    title:   qsTr("Simple GPS Map Demo")

    QlChannelSerial { id:serial }

    Component.onCompleted: {

            serial.open("ttymxc2");

            if (serial.isOpen()){

                serial.paramSet('baud',  '9600');
                serial.paramSet('bits',  '8');
                serial.paramSet('parity','n');
                serial.paramSet('stops', '1');

                serial.paramSet('dtr', '0');
                serial.paramSet('rts', '1');
            }
        }

    function setPosition(lat, lon) {
       var coord = QtPositioning.coordinate(lat,lon);
       myMap.center = coord;
       myMap.zoomLevel= 15
   }


   function parseLine(port_line){

       console.log(port_line);

       if(port_line.startsWith(String("$GPRMC"))){

           console.log("$GPRMC");

           myTimer.stringList = port_line.split(',');

           var     DD = parseInt(myTimer.stringList[3].substring(0,2));
           var     MM = parseFloat(myTimer.stringList[3].substring(3));
           console.log(myTimer.stringList[3]+"."+myTimer.stringList[4])

           var d;
           if(DD > 0)
               d = /*(Seconds/3600) +*/ (MM/60) + DD;
           else
               d = /*- (Seconds/3600)*/ - (MM/60) + DD;

           console.log(d);

           var DDD = parseInt(myTimer.stringList[5].substring(0,3));
           var MMM = parseFloat(myTimer.stringList[5].substring(4));
           console.log(myTimer.stringList[5]+"."+myTimer.stringList[6])

           var dd;
           if(DDD > 0)
               dd = /*(Seconds/3600) +*/ (MMM/60) + DDD;
           else
               dd = /*- (Seconds/3600)*/ - (MMM/60) + DDD;

           console.log(dd);

           setPosition(d, dd);

       }
   }

   // port poll timer
   Timer {id:myTimer; interval:50; running:true; repeat:true;

       property bool port_firstLine: true;
       property var  port_bytes: [];
       property var  port_line: [];
       property variant stringList;

       onTriggered: {
           if (serial.isOpen()){

               var bytes = serial.readBytes();

               if (bytes.length > 0){

                   port_bytes = port_bytes.concat(bytes);
                   var c = 0;

                   while ((port_bytes.length > 0) && ((c = String.fromCharCode(port_bytes.shift() & 0xFF)) != '\n')) port_line.push(c);

                   if (c === '\n'){
                       if (!port_firstLine){

                           parseLine(port_line.join(''));

                       }
                       else port_firstLine = false;

                       port_line = []; // clear line buffer
                   }

               }
           }
       }
   }

    Plugin {
        id: mapPlugin
        name: "osm" // "mapboxgl", "esri", ...
        // specify plugin parameters if necessary
        // PluginParameter {
        //     name:
        //     value:
        // }
    }

    Map {
        id: myMap;
        anchors.fill: parent
        plugin: mapPlugin
        zoomLevel: 14
    }





}
